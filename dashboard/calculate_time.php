<?php

header('Access-Control-Allow-Origin: *');

if (isset($_POST)) {
	$machine = $_GET['machine_name'];
	$date = $_GET['date'];
	$handle = curl_init();
	$url = "http://10.0.50.51:3000/logs?machine_name=".$machine."&date=" . $date;
	 
	curl_setopt($handle, CURLOPT_URL, $url);
	curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
	 
	$output = curl_exec($handle);
	 
	curl_close($handle);
 	$obj = json_decode($output, TRUE);

 	$array2 = [];
 	$computed = [];

 	foreach ($obj as $key => $value) {
 		if ($value['url'] === "newtab" || $value['url'] === 'extension') {
 			unset($obj[$key]);
 		}

 		if(isset($obj[$key + 1]['timestamp'])) {
 			$datetime1 = new DateTime($value['timestamp']);
			$datetime2 = new DateTime($obj[$key + 1]['timestamp']);
			$interval = $datetime1->diff($datetime2);
	 		$array2[$key]['url'] = $value['url'];
	 		$array2[$key]['time_spent'] = $interval->format('%H hrs(s) %i min(s) %s sec(s)');
 		}
		
	 }

 	echo json_encode($array2);
}