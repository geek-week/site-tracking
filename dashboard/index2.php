<html lang="en">
  <head>
    <?php include 'includes/css.php'; ?>
    <title>Automatic Site Logging</title>
  </head>
  <body style="background-color: #efefef">
    <div class="container-fluid" style="margin-top: 50px">
        <div class="row">
        	<div class="col-md-3">
	        	<div class="card">
				  <div class="card-body">
				    <form id="form_search" style="margin-bottom: 0">
		                <div class="input-group input-group mb-3">
		                    <input name="input_employee_id" maxlength="3" type="text" class="form-control numbers-only lt-num" placeholder="Enter Employee ID" />
		                </div>
		                <div class="input-group input-group mb-3">
		                    <input name="input_date" class="form-control" placeholder="Enter Date" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" />
		                </div>
		                <button style="background-color: #00cc99;border-color:#00cc99" class="btn btn-primary btn-block"><i class="fa fa-search mr-2"></i>Search</button>
		            </form>
				  </div>
				</div>
	        </div>

	        <div class="col-md-9" style="padding-left: 0">
	        	<div class="card">
	        		<div id="div_has_results" class="card-body" style="display:none;">
	        			<h1 id="p_emp_name">Karlo Dela Rosa</h1>
	        			<h6 id="p_emp_job" style="color:aaa">SDD</h6>
	        			<hr>
	        			<div class="row">
	        				<div class="col-md-8"><div id="container"></div></div>
	        				<div class="col-md-4">
	        					<h3>2018-12-23</h3>
	        					<hr>
	        					<div class="card">
	        						<div class="card-body">
	        							<div class="bd-example">
											<dl>
											  <dt>Stackoverflow</dt>
											  <dd>1hour 2mins</dd>
											  <dt>Facebook</dt>
											  <dd>1hour 2mins</dd>
											  <dt>Youtube</dt>
											  <dd>1hour 2mins</dd>
											</dl>
										</div>
	        						</div>
	        					</div>
	        				</div>
	        			</div>
	        			
	        			<script type="text/javascript">
	        				
							Highcharts.chart('container', {
							    chart: {
							        plotBackgroundColor: null,
							        plotBorderWidth: null,
							        plotShadow: false,
							        type: 'pie'
							    },
							    title: {
							        text: ''
							    },
							    tooltip: {
							        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
							    },
							    plotOptions: {
							        pie: {
							            allowPointSelect: true,
							            cursor: 'pointer',
							            dataLabels: {
							                enabled: true,
							                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
							                style: {
							                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
							                }
							            }
							        }
							    },
							    series: [{
							        name: 'Brands',
							        colorByPoint: true,
							        data: [{
							            name: 'Chrome',
							            y: 61.41,
							            sliced: true,
							            selected: true
							        }, {
							            name: 'Internet Explorer',
							            y: 11.84
							        }, {
							            name: 'Firefox',
							            y: 10.85
							        
							        }]
							    }]
							});
	        			</script>

	        		</div>
					<div id="div_no_results" class="card-body text-center">
						<p style="color: #9e9e9e; font-style: italic;">No results found.</p>
					</div>
	        	</div>
	        </div>
        </div>
    </div>
    <?php include 'includes/js.php'; ?>
  </body>
</html>