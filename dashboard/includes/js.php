<script src="./js/jquery-3.3.1.min.js"></script>
<script src="./js/popper.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/script.js"></script>

<script>
    $('#form_search').submit(function(event){
        event.preventDefault();
        var emp_id = $('input[name=input_employee_id]').val()
        if(emp_id){
            $.ajax({
                url: 'http://10.0.50.51:3000/user?employee_id='+emp_id,
                method: 'get',
                dataType: 'jsonp',
                success: function(response) {
                    $('#p_emp_name').html(response.name);
                    $('#p_emp_job').html(response.job);
                    $('#div_has_results').show();
                    $('#div_no_results').hide();
                },
                error: function(xhr, error){
                    console.log(xhr);
                    $('#div_has_results').hide();
                    $('#div_no_results').show();
                },
            });
        }

    });
</script>