<html lang="en">
  <head>
    <?php include 'includes/css.php'; ?>
    <title>Automatic Site Logging - Report</title>
  </head>
  <body>
    <div class="container-fluid">
        <?php
        if(!isset($_GET) || empty($_GET['input_employee_id']) || empty($_GET['input_date'])) { 
        ?>
        <div class="alert alert-danger mt-5" role="alert">
            No entered employee number or date!
        </div>
        <?php } else { ?>
        <div class="row justify-content-center mt-lg-5">
            <div class="col-lg-6">
                <p class="text-center">On <strong><?php echo date('F d, Y', strtotime($_GET['input_date'])) ?></strong></p>
                <h3 id="p_emp_name" class="text-center">Employee</h3>
                <p class="text-center">was on the following sites for this long:</p>
                <div class="mt-md-4" id="div_sites">
                    <div class="card mt-sm-3">
                        <div class="card-body">
                            <div class="row text-center">
                                <input type="checkbox" style="display: none;"/>
                                <div class="col-sm-6">
                                    <h5>Stackoverflow</h5>
                                </div>
                                <div class="col-sm-6">
                                    <h5><strong>1 hour and 36 mins</strong></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-sm-3">
                        <div class="card-body">
                            <div class="row text-center">
                                <input type="checkbox" style="display: none;"/>
                                <div class="col-sm-6">
                                    <h5>Google</h5>
                                </div>
                                <div class="col-sm-6">
                                    <h5><strong>1 hour and 23 mins</strong></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-sm-3">
                        <div class="card-body">
                            <div class="row text-center">
                                <input type="checkbox" style="display: none;"/>
                                <div class="col-sm-6">
                                    <h5>Facebook</h5>
                                </div>
                                <div class="col-sm-6">
                                    <h5><strong>57 mins</strong></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="input_machine_num" value=""/>

    <script src="./js/jquery-3.3.1.min.js"></script>
    <script src="./js/popper.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/script.js"></script>

    <script>

        var machine;

        $.ajax({
            url: 'http://10.0.50.51:3000/user?employee_id='+<?php echo $_GET['input_employee_id'] ?>,
            method: 'get',
            dataType: 'json',
            success: function(response) {
                $('#p_emp_name').html(response[0].name);
                getSites(response[0].machine_name);
            },
            error: function(xhr, error){
                console.log(xhr);
            },
        });
        

        function getSites(machine) {
            var url1 = 'http://10.0.50.51/automated-site-logging/dashboard/calculate_time.php?machine_name=';
            var url2 = '&date=<?php echo $_GET['input_date'] ?>';
            var url = url1.concat(machine, url2);
            console.log(url);
            $.ajax({
                url: url,
                method: 'get',
                dataType: 'json',
                success: function(response) {
                    var card = "";
                    for(var i=0; i < response.length; i++) {
                        
                        card += '<div class="card mt-sm-3"><div class="card-body"><div class="row text-center"><input type="checkbox" style="display: none;"/><div class="col-sm-6"><h5>'+response[i].url+'</h5></div><div class="col-sm-6"><h5><strong>'+response[i].time_spent+'</strong></h5></div></div></div></div>';
                    }
                    $('#div_sites').html(card);
                },
                error: function(xhr, error){
                    console.log(xhr);
                },
            });
        }

    </script>

    <?php } ?>

  </body>
</html>