<html lang="en">
  <head>
    <?php include 'includes/css.php'; ?>
    <title>Automatic Site Logging</title>
  </head>
  <body>
    <div class="container-fluid">
        <div class="row justify-content-center h-50 row align-items-center">
            <div class="col-lg-6">
                <form id="form_search" action="report.php" style="margin-bottom: 0">
                    <div class="input-group input-group mb-3">
                        <input name="input_employee_id" maxlength="3" type="text" class="form-control numbers-only lt-num" placeholder="Enter Employee ID" />
                    </div>
                    <div class="input-group input-group mb-3">
                        <input name="input_date" class="form-control" placeholder="Enter Date" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" />
                    </div>
                    <button style="background-color: #00cc99;border-color:#00cc99" class="btn btn-primary btn-block"><i class="fa fa-search mr-2"></i>Search</button>
                </form>
            </div>
        </div>
    </div>
  </body>
</html>