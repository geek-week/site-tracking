const express = require('express');
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.json());

let SiteLogModel = require('./models/site-logs.model');
let UserModel = require('./models/user.model');

app.post('/log', (request, response) => {
    if(Object.keys(request.body).length === 0) {
        return response.status(400).send('Request body is missing');
    }

    SiteLogModel.findOne({machine_name: request.body.machine_name}).sort({timestamp: -1}).exec((error, result) => {
        if(result){
            if(result.url == request.body.url){
                console.log('Still in the same URL');
                return response.status(400).send('Still in the same URL');
            }
        }

        let model = new SiteLogModel(request.body);
            model.save()
                .then(doc => {
                    if(!doc || doc.length === 0){
                        return response.status(400).send('Document empty');
                    }
                    console.log(doc);
                    return response.status(200).send(doc);
                })
                .catch(error => {
                    console.log(error);
                    return response.status(500).send(error);
                });
    });

    
});

app.get('/logs', (request, response) => {
    
    if(!request.query.machine_name){
        return response.status(400).send('Missing parameter');
    }

    SiteLogModel.find({machine_name: request.query.machine_name, date: request.query.date})
        .then(doc => {
            response.json(doc);
        })
        .catch(error => {
            response.status(500).send(error)
        });
});

app.get('/user', (request, response) => {

    response.header("Access-Control-Allow-Origin", "*");
    response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    if(!request.query.employee_id){
        return response.status(400).send('Missing parameter');
    }

    UserModel.find({employee_id: request.query.employee_id})
        .then(doc => {
            response.json(doc);
        })
        .catch(error => {
            response.status(500).send(error)
        });
});

app.listen(3000, () => console.log('Listening on port 3000...'));