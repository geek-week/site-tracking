let mongoose = require('mongoose');

const server = 'localhost:27017';
const database = 'automatedSiteTrackingDB';
const user = 'admin';
const password = '';

mongoose.connect(`mongodb://${server}/${database}`);

function convertTimestamp(date){
    return date.getTime();
}

let UserSchema = new mongoose.Schema({
    name: String,
    job: String,
    employee_id: String,
    machine_name: String
});

module.exports = mongoose.model('site_access_users', UserSchema);

