let mongoose = require('mongoose');

const server = 'localhost:27017';
const database = 'automatedSiteTrackingDB';
const user = 'admin';
const password = '';

mongoose.connect(`mongodb://${server}/${database}`);

function convertDate(date) {
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth()+1).toString();
    var dd  = date.getDate().toString();
  
    var mmChars = mm.split('');
    var ddChars = dd.split('');
  
    return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
}

let SiteLogSchema = new mongoose.Schema({
    machine_name: String,
    url: String,
    date: {
        type: String,
        default: convertDate(new Date)
    },
    timestamp: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('site_access_logs', SiteLogSchema);

