chrome.tabs.onUpdated.addListener( function (tabId, changeInfo, tab) {
	if (changeInfo.status == 'complete'  && tab.active) {
		chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
		    var url = tabs[0].url;
		    var urls2 = new URL(url);
  			var domain = urls2.hostname;
  			getPcNameAndUsername(domain);
		});
	}
});

function insertToDb(url,pcname) {
	var arr = {url: url, machine_name: pcname};
	$.ajax({
		url: 'http://10.0.50.51:3000/log',
		method: 'post',
		contentType: "application/json",
		dataType: 'json',
		data : JSON.stringify(arr),
		success: function(response) {
			alert(response);
		},
		error: function(xhr, error){
		    alert(xhr.responseText);
		 },
	})
}

function getPcNameAndUsername(domain) {
	$.ajax({
		url: 'http://10.0.50.40/log2/php/pcname_and_username.php',
		method: 'post',
		success: function(response) {
			var result = $.parseJSON(response);
			insertToDb(domain,result.pcname);
		}
	})
}
